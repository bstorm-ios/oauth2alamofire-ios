# Oauth2Alamofire

[![CI Status](http://img.shields.io/travis/sima94/Oauth2Alamofire.svg?style=flat)](https://travis-ci.org/sima94/Oauth2Alamofire)
[![Version](https://img.shields.io/cocoapods/v/Oauth2Alamofire.svg?style=flat)](http://cocoapods.org/pods/Oauth2Alamofire)
[![License](https://img.shields.io/cocoapods/l/Oauth2Alamofire.svg?style=flat)](http://cocoapods.org/pods/Oauth2Alamofire)
[![Platform](https://img.shields.io/cocoapods/p/Oauth2Alamofire.svg?style=flat)](http://cocoapods.org/pods/Oauth2Alamofire)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Oauth2Alamofire is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Oauth2Alamofire'
```

## Author

sima94, stefan.simic@bstorm.co

## License

Oauth2Alamofire is available under the MIT license. See the LICENSE file for more info.
