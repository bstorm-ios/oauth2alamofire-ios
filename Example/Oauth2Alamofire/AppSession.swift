//
//  AppSession.swift
//

import Foundation
import UIKit
import Oauth2Alamofire

class AppSession: AlamofireOauthManagerDelegate {
    
    static let shared = AppSession()
    
    var _oauth: AppOauth!
    var oauth: Oauth{
        get {
            _oauth.loadFromKeychain()
            return _oauth
        }
        set{
            if _oauth == nil {
                _oauth = (newValue as! AppOauth)
            } else {
                _oauth.update(oauth: newValue)
            }
            _oauth.saveToLocksmith()
        }
    }
    
    func oauthValidationDidFailed(with error: Error) {
        forceLogout()
    }

    func autoLogin(){
        
    }
    
    func login(username: String, password: String, completion: @escaping (Any?)->Void){

        AlamofireOauth.login(with: "", password: "", grantType: .password, headers: [:]) { (oauthResponse) in
            
        }
        
        AlamofireOauth.login(with: username, password: password) { (oauthResponse) in
            
        }
    }
    
    func logout() {

    }
    
    func forceLogout(){
        
    }
}
