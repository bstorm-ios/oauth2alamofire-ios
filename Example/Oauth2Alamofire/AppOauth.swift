//
//  AppOauth.swift
//

import Foundation
import Oauth2Alamofire
import Locksmith

class AppOauth: Oauth{

    func loadFromKeychain(for key: String = "oauthKeychain") {
        
        let dic = Locksmith.loadDataForUserAccount(userAccount: key) ?? [:]
        username = dic["username"] as? String
        accessToken = dic["accessToken"] as? String
        refreshToken = dic["refreshToken"] as? String
        expiresIn = dic["expiresIn"] as? Double ?? 0
        tokenType = dic["tokenType"] as? String ?? ""
        scope  = dic["scope"] as? String
        
    }
    
    func saveToLocksmith(for key: String = "oauthKeychain") {
        try? Locksmith.deleteDataForUserAccount(userAccount: key)
        try! Locksmith.saveData(data: data, forUserAccount: key)
    }
    
    func delete(for key: String = "oauthKeychain") {
        
        do{
           try Locksmith.deleteDataForUserAccount(userAccount: key)
           username = nil
           accessToken = nil
           refreshToken = nil
           expiresIn = 0
           tokenType = nil
           scope = nil
           saveToLocksmith()
        }catch{ }
    }
    
    // Required by CreateableSecureStorable
    var data: [String: Any] {
        var dic = [String:Any]()
        
        dic["username"] = username
        dic["accessToken"] = accessToken
        dic["refreshToken"] = refreshToken
        dic["expiresIn"] = expiresIn
        dic["tokenType"] = tokenType
        dic["scope"] = scope
        
        return dic
    }
    
    override func update(params: [AnyHashable : Any], headers: [String : Any]) {
        tokenType = params["token_type"] as? String
        expiresIn = Date().timeIntervalSince1970 + (params["expires_in"] as? Double ?? 0)
        accessToken = params["access_token"] as? String
        refreshToken = params["refresh_token"] as? String
    }
    
    override func loginParams() -> [String: Any]{
        var params = [String: Any]()
        params["client_id"] = clientId
        params["client_secret"] = clientSecret
        params["grant_type"] = GrantType.password.rawValue
        params["username"] = username
        params["password"] = password
        return params
    }
    
    override func refreshParams() -> RefreshTokenParametars {
        var params = [String: Any]()
        params["client_id"] = clientId
        params["client_secret"] = clientSecret
        params["grant_type"] = GrantType.refreshToken.rawValue
        params["refresh_token"] = refreshToken
        return (params: params, [:])
    }
}
