//
//  Oauth.swift
//

import Foundation
import Alamofire

public typealias RefreshTokenParametars = (params: [String: Any], header: HTTPHeaders)

public enum GrantType: String {
    case password = "password"
    case refreshToken = "refresh_token"
}

open class Oauth {
    
    public var clientId: String?
    public var clientSecret: String?
    public var grantType: GrantType?
    public var username: String?
    public var password: String?

    public var accessToken: String?
    public var expiresIn: Double = 0
    
    public var tokenType: String?
    public var scope: String?
    public var refreshToken: String?
    
    open var authorizationToken: HTTPHeaders{
        let tokenType = self.tokenType ?? ""
        let accessToken = self.accessToken ?? ""
        var token = tokenType + " " + accessToken
        if accessToken.count == 0 { token = "" }
        return ["Authorization": token]
    }
    
    public func isAccessTokenNeearToExpired(nearSec: Double = 30) -> Bool {
        let nowSek = Date().timeIntervalSince1970
        return nowSek - nearSec >= expiresIn
    }
    
    public var isTokensExists: Bool {
        return accessToken != nil && refreshToken != nil
    }
    
    public init(){
        self.clientId = ""
        self.clientSecret = ""
    }
    
    public init(clientId: String, clientSecret: String){
        self.clientId = clientId
        self.clientSecret = clientSecret
    }
    
    open func update(oauth: Oauth){
        
         clientId = oauth.clientId ?? clientId
         clientSecret = oauth.clientSecret ?? clientSecret
        
         grantType = oauth.grantType ?? grantType
         username = oauth.username ?? username
         password = oauth.password ?? password
        
         accessToken = oauth.accessToken ?? accessToken
         expiresIn = oauth.expiresIn 
        
         tokenType = oauth.tokenType ?? tokenType
         scope = oauth.scope ?? scope
         refreshToken = oauth.refreshToken ?? refreshToken
    }
    
    open func update(params: [AnyHashable: Any], headers: [String: Any]){
        assertionFailure("Not implemented!")
    }
    
    open func loginParams() -> [String: Any]{
        assertionFailure("Not implemented!")
        return [:]
    }
    open func refreshParams() -> RefreshTokenParametars{
        assertionFailure("Not implemented!")
        return (params: [:], [:])
    }
}
