//
//  AlamofireOauthConfiguration.swift
//

import Foundation
import Alamofire

public class AlamofireOauthConfiguration {
    
    public var loginUrl: String
    public var refreshTokenUrl: String
    public var defaultHeaders: HTTPHeaders
    
    public init(loginUrl:String, refreshTokenUrl: String, defaultHeaders: HTTPHeaders ) {
        self.loginUrl = loginUrl
        self.refreshTokenUrl = refreshTokenUrl
        self.defaultHeaders = defaultHeaders
    }
}
