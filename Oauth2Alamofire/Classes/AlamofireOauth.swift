//
//  AlamofireOauth.swift
//

import Foundation
import Alamofire

public typealias AlamofireOauthResponse = (OauthResponse) -> Void

public var AlamofireOauth: AlamofireOauthSessionManager {
    return AlamofireOauthSessionManager.shared
}

enum StatusCode: Int {
    case badRequest           = 400
    case invalideAccessToken  = 401
    case serverError          = 500
    case noConnection         = -1009
}

public enum OauthResponse {
    case success(Oauth, [AnyHashable:Any])
    case failure(Error, [AnyHashable:Any])
}

public protocol AlamofireOauthManagerDelegate: class {
    var oauth: Oauth { get set }
    func oauthValidationDidFailed(with error: Error)
}

open class AlamofireOauthSessionManager: SessionManager, RequestRetrier, RequestAdapter  {
    
    public private(set) static var shared: AlamofireOauthSessionManager = AlamofireOauthSessionManager()
    private var configuration: URLSessionConfiguration
    public var oauthConfiguration: AlamofireOauthConfiguration!
    
    private let lock = NSLock()
    open var isRefreshing = false
    
    open weak var oauthDelegate: AlamofireOauthManagerDelegate?
    
    public override init(configuration: URLSessionConfiguration = URLSessionConfiguration.default, delegate: SessionDelegate = SessionDelegate(), serverTrustPolicyManager: ServerTrustPolicyManager? = nil) {
        
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        self.configuration = configuration
        //self.oauthConfiguration = AlamofireOauthConfiguration()
        super.init(configuration: configuration, delegate: delegate, serverTrustPolicyManager: serverTrustPolicyManager)
        adapter = self
        retrier = self
    }
    
    public static func setDefaultSession(session: AlamofireOauthSessionManager){
        shared = session
    }
    
    public func config(with config: AlamofireOauthConfiguration, oauth: Oauth, delegate: AlamofireOauthManagerDelegate) {
        oauthConfiguration = config
        oauthDelegate = delegate
        oauthDelegate?.oauth = oauth
    }
    
    public func login(with username: String, password: String, grantType: GrantType = .password, headers: HTTPHeaders? = nil, completion: @escaping AlamofireOauthResponse){
        guard let loginOauth = oauthDelegate?.oauth else { return }
        
        loginOauth.username = username
        loginOauth.password = password
        loginOauth.grantType = grantType
        
        let params = loginOauth.loginParams()
        Alamofire.request(oauthConfiguration.loginUrl, method: .post, parameters: params, headers: headers).validate(statusCode: 200..<300).responseJSON {[weak self] (dataResponse) in
            self?.oauthDataResponseHandler(dataResponse: dataResponse, type: .password, completion: completion)
        }
    }
    
    open func refreshToken(completion: @escaping AlamofireOauthResponse) {
        guard !isRefreshing else { return }
        isRefreshing = true
         guard let oauth = oauthDelegate?.oauth else { return }
         let refresh = oauth.refreshParams()
         Alamofire.request(oauthConfiguration.refreshTokenUrl, method: .post, parameters: refresh.params, headers: refresh.header).validate(statusCode: 200..<300).responseJSON {[weak self] (dataResponse) in
            guard let strongSelf = self else { return }
            strongSelf.oauthDataResponseHandler(dataResponse: dataResponse, type: .refreshToken, completion: completion)
            strongSelf.isRefreshing = false
         }
    }
    
    open func oauthDataResponseHandler(dataResponse: DataResponse<Any>,type: GrantType, completion: @escaping AlamofireOauthResponse){
        guard let oauth = oauthDelegate?.oauth else { return }
        switch dataResponse.result {
        case .success(let data):
            let json = data as! [AnyHashable:Any]
            oauth.update(params: json, headers: dataResponse.response?.allHeaderFields as? [String : Any] ?? [:])
            self.oauthDelegate?.oauth = oauth
            completion(.success(oauth, json))
        case .failure(let error):
            if type == .refreshToken {
                oauthErrorHandler(error: error, statusCode: dataResponse.response?.statusCode ?? StatusCode.invalideAccessToken.rawValue)
            }
            let data = dataResponse.data ?? Data()
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                completion(.failure(error, json as? [AnyHashable:Any] ?? [:]))
            } catch _ {
                completion(.failure(error, [:]))
            }
        }
    }
    
    open func oauthErrorHandler(error: Error, statusCode: Int)  {
        switch statusCode {
        case StatusCode.badRequest.rawValue ..< StatusCode.serverError.rawValue:
            oauthDelegate?.oauthValidationDidFailed(with: error)
        default: break
        }
    }
    
    open func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        lock.lock() ; defer { lock.unlock() }
        
        if let response = request.task?.response as? HTTPURLResponse, StatusCode.invalideAccessToken.rawValue == response.statusCode, let _ = request.request {
            if isRefreshing { return }
            refreshToken {[weak self] (oauthResponse) in
                guard let strongSelf = self else { return }
                
                strongSelf.lock.lock() ; defer { strongSelf.lock.unlock() }
                switch oauthResponse {
                case .success(_):
                    completion(true, 0.0)
                case .failure(_, _):
                    completion(false, 0.0)
                }
            }
        }else {
            completion(false, 0.0)   // Not a 401, not our problem
        }
    }
    
    open func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest
        
        oauthConfiguration?.defaultHeaders.forEach { (k,v) in
            urlRequest.setValue(v, forHTTPHeaderField: k)
        }
        
        guard let oauth = oauthDelegate?.oauth else { return urlRequest }
        oauth.authorizationToken.forEach { (k,v) in
            if v != "" {
               urlRequest.setValue(v, forHTTPHeaderField: k)
            }
        }
        return urlRequest
    }
}

